# Bitbucket Pipelines PHP 8.0 Docker Image

This is an image that can build, test and deploy a PHP application (mainly built for Laravel).

Included are the following:

- curl, rsync, zip, sqlite3
- NodeJS 10.x
- PHP 8.0 with the following modules:
    - php8.0-cli
    - php8.0-mbstring
    - php8.0-curl
    - php8.0-xml
    - php8.0-bcmath
    - php8.0-mysql
    - php8.0-gd
    - php8.0-sqlite3
    - php8.0-zip
    - php8.0-xdebug
    - php8.0-intl
    - php8.0-mcrypt
- AWS CLI tools
