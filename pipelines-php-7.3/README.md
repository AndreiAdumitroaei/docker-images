# Bitbucket Pipelines PHP 7.3 Docker Image

This is an image that can build, test and deploy a PHP application (mainly built for Laravel).

Included are the following:

- curl, rsync, zip, sqlite3
- NodeJS 10.x
- PHP 7.3 with the following modules:
    - php7.3-cli
    - php7.3-mbstring
    - php7.3-json
    - php7.3-curl
    - php7.3-xml
    - php7.3-bcmath
    - php7.3-mysql
    - php7.3-gd
    - php7.3-sqlite3
    - php7.3-zip
    - php7.3-intl
- AWS CLI tools
