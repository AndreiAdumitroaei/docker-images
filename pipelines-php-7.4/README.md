# Bitbucket Pipelines PHP 7.4 Docker Image

This is an image that can build, test and deploy a PHP application (mainly built for Laravel).

Included are the following:

- curl, rsync, zip, sqlite3
- NodeJS 10.x
- PHP 7.4 with the following modules:
    - php7.4-cli
    - php7.4-mbstring
    - php7.4-json
    - php7.4-curl
    - php7.4-xml
    - php7.4-bcmath
    - php7.4-mysql
    - php7.4-gd
    - php7.4-sqlite3
    - php7.4-zip
    - php7.4-xdebug
    - php7.4-intl
- AWS CLI tools
